
#%% [markdown]
# https://www.kaggle.com/c/digit-recognizer/overview
### MNIST ("Modified National Institute of Standards and Technology") 
# is the de facto “hello world” dataset of computer vision. Since its release in 1999, this classic dataset of handwritten images has served as the basis for benchmarking classification algorithms. As new machine learning techniques emerge, MNIST remains a reliable resource for researchers and learners alike.<br><br>

# In this competition, your goal is to correctly identify digits from a dataset of tens of thousands of handwritten images. We’ve curated a set of tutorial-style kernels which cover everything from regression to neural networks. We encourage you to experiment with different algorithms to learn first-hand what works well and how techniques compare.<br><br>

# ***The data files train.csv and test.csv contain gray-scale images of hand-drawn digits, from zero through nine.***<br><br>

# Each image is 28 pixels in height and 28 pixels in width, for a total of 784 pixels in total. Each pixel has a single pixel-value associated with it, indicating the lightness or darkness of that pixel, with higher numbers meaning darker. This pixel-value is an integer between 0 and 255, inclusive.<br>

# The training data set, (train.csv), has 785 columns. The first column, called "label", is the digit that was drawn by the user. The rest of the columns contain the pixel-values of the associated image.<br>

# Each pixel column in the training set has a name like pixelx, where x is an integer between 0 and 783, inclusive. To locate this pixel on the image, suppose that we have decomposed x as x = i * 28 + j, where i and j are integers between 0 and 27, inclusive. Then pixelx is located on row i and column j of a 28 x 28 matrix, (indexing by zero).<br>

# - For example, pixel31 indicates the pixel that is in the fourth column from the left, and the second row from the top, as in the ascii-diagram below.

# - Visually, if we omit the "pixel" prefix, the pixels make up the image like this:

# 000 001 002 003 ... 026 027<br>
# 028 029 030 031 ... 054 055<br>
# 056 057 058 059 ... 082 083<br>
#  |   |   |   |  ...  |   |<br>
# 728 729 730 731 ... 754 755<br>
# 756 757 758 759 ... 782 783<br><br><br>
# - The test data set, (test.csv), is the same as the training set, except that it does not contain the "label" column.

# - Your submission file should be in the following format: For each of the 28000 images in the test set, output a single line containing the ImageId and the digit you predict. For example, if you predict that the first image is of a 3, the second image is of a 7, and the third image is of a 8, then your submission file would look like:

# ImageId,Label<br>
# 1,3<br>
# 2,7<br>
# 3,8 <br>
# (27997 more lines)<br><br>
# - The evaluation metric for this contest is the categorization accuracy, or the proportion of test images that are correctly classified. For example, a categorization accuracy of 0.97 indicates that you have correctly classified all but 3% of the images.<br>

#--------



# %%
import os, sys, re, codecs, string, random 
import pandas as pd
import numpy as np
import sklearn, nltk
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import matplotlib.pyplot as plt
from nltk.stem.porter import PorterStemmer

# This is a bit of magic to make matplotlib figures appear inline in the notebook
# rather than in a new window.
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['figure.figsize'] = (10.0, 8.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# Some more magic so that the notebook will reload external python modules;
# see http://stackoverflow.com/questions/1907993/autoreload-of-modules-in-ipython
get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')


# %%
D_PATH = "./Kaggle/DigitRecognizer/"
D_TRAIN = "train.csv"
D_TEST = "test.csv"

train_df = pd.read_csv(D_PATH+D_TRAIN)
test_df = pd.read_csv(D_PATH+D_TEST)
print(train_df.shape)
print(train_df.columns)

## take a quick look at the data
print(train_df.head())
print(train_df.describe())
print(train_df.groupby("label"))

#%%
## clean up NAN for both train and test datesets
for df in [train_df, test_df][:]:
    series_nan = df.isna().any()
    idx = series_nan.index
    series_nan = [i for i in idx if series_nan[i]==True]

    print("Columns with NAN ", series_nan) 

    ## replace NAN with max count 
    dict_NAN = {}
    for c in series_nan:
        counts = train_df[c].value_counts()
        dict_NAN[c] = counts.index[0]
    
    df.fillna(dict_NAN, inplace=True)


#%% [markdown]
# https://towardsdatascience.com/support-vector-machine-introduction-to-machine-learning-algorithms-934a444fca47
# https://www.csie.ntu.edu.tw/~cjlin/papers/libsvm.pdf

## Support Vector Machine 
#  support vector machine algorithm tries to find a hyperplane in an $N$-dimensional space($N$ being the number of features) that distinctly classifies the data points.

# <img src="./Kaggle/DigitRecognizer/SVM.png" alt="IMG">

# - To separate the two classes of data points, there are many possible hyperplanes that could be chosen. Our objective is to find a plane that has the maximum margin, i.e the maximum distance between data points of both classes. Maximizing the margin distance provides some reinforcement so that future data points can be classified with more confidence.

# - Hyperplanes are decision boundaries that help classify the data points. Data points falling on either side of the hyperplane can be attributed to different classes.

## Cost Function and Gradient Updates
# - In the SVM algorithm, we are looking to maximize the margin between the data points and the hyperplane. The loss function that helps maximize the margin is hinge loss.
#  - $L (X, y, f(X)) = max(0, 1-y.f(X)) $


# %%
# Import datasets, classifiers and performance metrics
from sklearn import datasets, svm, metrics
from sklearn.model_selection import train_test_split

# The digits dataset
digits = train_df

## 
target = "label"
features = list(train_df.columns)
features.remove(target)

## Let's take a look at 10 random images
sample_images = train_df.sample(10)
images = sample_images[features].values
labels = sample_images[[target]].values

print(images[0].shape) 
_, axes = plt.subplots(2, 5)
print(len(axes[0, :]))
images_and_labels = list(zip(images, labels))
for ax, (image, label) in zip(axes[0, :], images_and_labels[:5]):
    ax.set_axis_off()
    ax.imshow(image.reshape(28, 28), cmap=plt.cm.gray_r, interpolation='nearest')
    ax.set_title('Training: %i' % label)

# To apply a classifier on this data, we need to flatten the image, to
# turn the data in a (samples, feature) matrix:
n_samples = train_df.shape[0]
data = train_df

# Create a classifier: a support vector classifier
classifier = svm.SVC(gamma=0.001)

# Split data into train and test subsets
X_train, X_test, y_train, y_test = train_test_split(
    data, (train_df.label).values, test_size=0.2, shuffle=False)

# We learn the digits on the first half of the digits
classifier.fit(X_train, y_train)

#%%
# Now predict the value of the digit on the second half:
predicted = classifier.predict(X_test)

images_and_predictions = list(zip(digits.images[n_samples // 2:], predicted))
for ax, (image, prediction) in zip(axes[1, :], images_and_predictions[:4]):
    ax.set_axis_off()
    ax.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    ax.set_title('Prediction: %i' % prediction)

print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(y_test, predicted)))
disp = metrics.plot_confusion_matrix(classifier, X_test, y_test)
disp.figure_.suptitle("Confusion Matrix")
print("Confusion matrix:\n%s" % disp.confusion_matrix)

plt.show()


# %%
