# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% Change working directory from the workspace root to the ipynb file location. Turn this addition off with the DataScience.changeDirOnImportExport setting
# ms-python.python added
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'Courses/Kaggle/DisasterTweets'))
	print(os.getcwd())
except:
	pass
# %%
from IPython import get_ipython


# %%


# %% [markdown]
# #%% [markdown]
# 
# ## Disaster Tweets 
# https://www.kaggle.com/c/nlp-getting-started/overview
# 
# This particular challenge is perfect for data scientists looking to get started with Natural Language Processing. The competition dataset is not too big, and even if you don’t have much personal computing power, you can do all of the work in our free, no-setup, Jupyter Notebooks environment called Kaggle Notebooks.
# 
# ## Competition Description
# 
# Twitter has become an important communication channel in times of emergency.
# The ubiquitousness of smartphones enables people to announce an emergency they’re observing in real-time. Because of this, more agencies are interested in programatically monitoring Twitter (i.e. disaster relief organizations and news agencies).
# 
# But, it’s not always clear whether a person’s words are actually announcing a disaster. 
# 
# ## What files do I need?
# 
# You'll need train.csv, test.csv and sample_submission.csv.
# 
# What should I expect the data format to be?
# 
# Each sample in the train and test set has the following information:
# 
# The text of a tweet
# A keyword from that tweet (although this may be blank!)
# The location the tweet was sent from (may also be blank)
# What am I predicting?
# 
# You are predicting whether a given tweet is about a real disaster or not. If so, predict a 1. If not, predict a 0.
# 
# ## Files
# 
# train.csv - the training set
# test.csv - the test set
# sample_submission.csv - a sample submission file in the correct format
# Columns
# 
# id - a unique identifier for each tweet
# text - the text of the tweet
# location - the location the tweet was sent from (may be blank)
# keyword - a particular keyword from the tweet (may be blank)
# target - in train.csv only, this denotes whether a tweet is about a real disaster (1) or not (0)
# 

# %%
import os, sys, re, codecs, string
import pandas as pd
import numpy as np
import sklearn, nltk
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import matplotlib.pyplot as plt
from nltk.stem.porter import PorterStemmer

## After installation, you will need to install the data used with the library, including a great set of documents that you can use later for testing other tools in NLTK.
# nltk.download('stopwords')


# This is a bit of magic to make matplotlib figures appear inline in the notebook
# rather than in a new window.
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['figure.figsize'] = (10.0, 8.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# Some more magic so that the notebook will reload external python modules;
# see http://stackoverflow.com/questions/1907993/autoreload-of-modules-in-ipython
get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')


# %%
## consts 
STOP_WORDS = stopwords.words("english")

# %%
## Prepare data
D_PATH = "./Kaggle/DisasterTweets/"
D_TRAIN = "train.csv"
D_TEST = "test.csv"

train_df = pd.read_csv(D_PATH+D_TRAIN)
test_df = pd.read_csv(D_PATH+D_TEST)
print(train_df.shape)
print(train_df.columns)

## deal with the categorical data & NAN
print(train_df.head())
print(train_df.describe())
print(train_df.groupby("target").count())

# %% [markdown]
# #%% [markdown]
# ## First clean the data
# - Remove all irrelevant characters such as any non alphanumeric characters
# - Tokenize your text by separating it into individual words
# - Remove words that are not relevant, such as “@” twitter mentions or urls
# - Convert all characters to lowercase, in order to treat words such as “hello”, “Hello”, and “HELLO” the same
# - Consider combining misspelled or alternately spelled words to a single representation (e.g. “cool”/”kewl”/”cooool”)
# - Consider lemmatization (reduce words such as “am”, “are”, and “is” to a common form such as “be”)
# 
# ## Stemming/lemmatization refers to the process of reducing each word to its root or base.
# 
# For example “fishing,” “fished,” “fisher” all reduce to the stem “fish.”
# 
# Some applications, like document classification, may benefit from stemming in order to both reduce the vocabulary and to focus on the sense or sentiment of a document rather than deeper meaning.
# 
# There are many stemming algorithms, although a popular and long-standing method is the Porter Stemming algorithm. This method is available in NLTK via the PorterStemmer class.
# 
# ## Additional Text Cleaning Considerations
# 
# Here is a short list of additional considerations when cleaning text:
# 
# 1. Handling large documents and large collections of text documents that do not fit into memory.
# 2. Extracting text from markup like HTML, PDF, or other structured document formats.
# 3. Transliteration of characters from other languages into English.
# 4. Decoding Unicode characters into a normalized form, such as UTF8.
# 5. Handling of domain specific words, phrases, and acronyms.
# 6. Handling or removing numbers, such as dates and amounts.
# 7. Locating and correcting common typos and misspellings.

# %%
def clean_text(text):
    """ tokenize, basic cleaning, lower casing and steming ...
    """
    tkns = word_tokenize(text)    
    tkns = [w.lower() for w in tkns if (w not in STOP_WORDS and w.isalpha())]

    ## just interested in the stem
    porter = PorterStemmer()
    tkns = [porter.stem(w) for w in tkns]
    return tkns

## clean train and test data
train_df["text"] = train_df["text"].apply(lambda x: clean_text(x))
test_df["text"] = test_df["text"].apply(lambda x: clean_text(x))

# %%
## drop unusable columns
# DROP_COLUMNS = [""]
# train_df.drop(columns=DROP_COLUMNS, inplace=True)
# test_df.drop(columns=DROP_COLUMNS, inplace=True)


#%% [markdown]
## Feature Engineering
# In this section we build features for training classifiers 
#### High level features:
# - id
# - location
# - text length:       

train_df["text_length"] = train_df["text"].apply(lambda x: len(x))
test_df["text_length"] = test_df["text"].apply(lambda x: len(x))

correct_tweets = train_df[train_df["target"]==0]
incorrect_tweets = train_df[train_df["target"]==1]

print(train_df.groupby("location").count())

# %% [markdown]
## TOverlay high level features to see variation with the class type

# %%
def overlay_feats(class_dfs, features):
    for feat in features:
        fig = plt.figure(figsize=(10, 10)) 
        for i, class_df in enumerate(class_dfs):
            plt.hist(class_df[feat], histtype="step", label="Class %i"%i, density=True)
        # plt.hist(incorrect_tweets["text_length"], color="r", histtype="step",  label="Incorrect", density=True,)


        plt.xlabel(feat)
        plt.ylabel('# of tweets')
        plt.legend(loc="best")
        plt.show()


features = ["text_length", "id"]
class_dfs = [correct_tweets, incorrect_tweets]
overlay_feats(class_dfs, features)


# %% [markdown]
## Low level features
# In order to perform machine learning on text documents, we first need to turn the 
# text content into numerical feature vectors.# - parsing the tweet content to extract meaningful information for classification
## Bags of words
####The most intuitive way to do so is to use a bags of words representation:

# 1. Assign a fixed integer id to each word occurring in any document of the training set (for instance by building a dictionary from words to integer indices).

# 2. For each document #i, count the number of occurrences of each word w and store it in X[i, j] as the value of feature #j where j is the index of word w in the dictionary.

### The bags of words representation implies that n_features is the number of distinct words in the corpus: this number is typically larger than 100,000.

# - If n_samples == 10000, storing X as a NumPy array of type float32 would require 10000 x 100000 x 4 bytes = 4GB in RAM which is barely manageable on today’s computers.

# - Fortunately, most values in X will be zeros since for a given document less than a few thousand distinct words will be used. For this reason we say that bags of words are typically high-dimensional sparse datasets. We can save a lot of memory by only storing the non-zero parts of the feature vectors in memory.

# - scipy.sparse matrices are data structures that do exactly this, and scikit-learn has built-in support for these structures.

## Insights into bag of words
# - The BOW model only considers if a known word occurs in a document or not. It does not care about meaning, context, and order in which they appear.

# - This gives the insight that similar documents will have word counts similar to each other. In other words, the more similar the words in two documents, the more similar the documents can be.

## Limitations of BOW
# - Semantic meaning: the basic BOW approach does not consider the meaning of the word in the document. It completely ignores the context in which it’s used. The same word can be used in multiple places based on the context or nearby words.
# - Vector size: For a large document, the vector size can be huge resulting in a lot of computation and time. You may need to ignore words based on relevance to your use case.

## Words --> Features
# we can define a feature for each word, indicating whether a weet contains that word. To limit the number of features that the classifier needs to process, we begin by constructing a list of the <200> most frequent words in the overall corpus. We can then define a feature extractor that simply checks whether each of these words is present in a given document.

# %% [markdown]
## Word Frequency Distribution 
# 
#  
# %%
corpus_words = []
for ws in train_df["text"]:
    corpus_words += ws
freq_list = nltk.FreqDist(w for w in corpus_words)
freq_list.plot(50)

## get the most 200 common words to build the features 
freq_wds = freq_list.most_common(200)
most_common = [fw[0] for fw in freq_wds]
print(freq_wds)

# %% [markdown]
## Check each tweet to see if contains words from the most_common words

# %%
def text_features(text):
    features = {}
    for word in text:
        features[word] = (word in most_common)
    return features

ll_features = [(text_features(t), c) for (t, c) in zip(train_df.text, train_df.target)]

# %% [markdown]
## Train Naive Bayes Classifier with nltk
# - source: https://scikit-learn.org/stable/modules/naive_bayes.html
# - Naive Bayes methods are a set of supervised learning algorithms based on applying Bayes’ theorem with the “naive” assumption of conditional independence between every pair of features given the value of the class variable.

# - The different naive Bayes classifiers differ mainly by the assumptions they make regarding the distribution of 
# .

# - In spite of their apparently over-simplified assumptions, naive Bayes classifiers have worked quite well in many real-world situations, famously document classification and spam filtering. They require a small amount of training data to estimate the necessary parameters. (For theoretical reasons why naive Bayes works well, and on which types of data it does, see the references below.)

# - Naive Bayes learners and classifiers can be extremely fast compared to more sophisticated methods. The decoupling of the class conditional feature distributions means that each distribution can be independently estimated as a one dimensional distribution. This in turn helps to alleviate problems stemming from the curse of dimensionality.

# - On the flip side, although naive Bayes is known as a decent classifier, it is known to be a bad estimator, so the probability outputs from predict_proba are not to be taken too seriously.


# %%
## train set and validation set
train_set, val_set = ll_features[100:], ll_features[:100]
classifier = nltk.NaiveBayesClassifier.train(train_set)

classifier.show_most_informative_features(20)
print(nltk.classify.accuracy(classifier, val_set))

# %% [markdown]
## Train classifier with only the high level features using sklearn
from sklearn.naive_bayes import GaussianNB
X = train_df[["id", "text_length"]].values
y = train_df[["target"]].values
X_tr, X_vl = X[100:], X[:100]
y_tr, y_vl = y[100:], y[:100]

nb_model = GaussianNB()
nb_model.fit(X_tr, y_tr)
nb_model.score(X_vl, y_vl)

# %% [markdown]
## Train classifier with only the low level level features using sklearn
# https://scikit-learn.org/stable/tutorial/text_analytics/working_with_text_data.html
### Tokenizing text with scikit-learn
# - Text preprocessing, tokenizing and filtering of stopwords are all included in CountVectorizer, which builds a dictionary of features and transforms documents to feature vectors:
# - CountVectorizer supports counts of N-grams of words or consecutive characters. Once fitted, the vectorizer has built a dictionary of feature indices:

## From occurrences to frequencies
# - Occurrence count is a good start but there is an issue: longer documents will have higher average count values than shorter documents, even though they might talk about the same topics.

# - To avoid these potential discrepancies it suffices to divide the number of occurrences of each word in a document by the total number of words in the document: these new features are called tf for Term Frequencies.

# - Another refinement on top of tf is to downscale weights for words that occur in many documents in the corpus and are therefore less informative than those that occur only in a smaller portion of the corpus.

# - This downscaling is called tf–idf for “Term Frequency times Inverse Document Frequency”.


#%% 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

train_df = pd.read_csv(D_PATH+D_TRAIN)
test_df = pd.read_csv(D_PATH+D_TEST)

count_vect = CountVectorizer(stop_words="english", strip_accents="ascii")
X_train_counts = count_vect.fit_transform(train_df.text)
print(count_vect.get_feature_names())

tfidf_transformer = TfidfTransformer(use_idf=False).fit(X_train_counts)
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)

print(X_train_tfidf.shape)

# %% [markdown]
# Now that we have our features, we can train a classifier to try to predict the category of a post. Let’s start with a naïve Bayes classifier, which provides a nice baseline for this task. scikit-learn includes several variants of this classifier; the one most suitable for word counts is the multinomial variant:<br>
# ---
## MultinomialNB:
# -  implements the naive Bayes algorithm for multinomially distributed data, and is one of the two classic naive Bayes variants used in text classification (where the data are typically represented as word vector counts, although tf-idf vectors are also known to work well in practice). 
# - The distribution is parametrized by vectors 
#  for each class , where  is the number of features (in text classification, the size of the vocabulary) and  is the probability 
#  of feature appearing in a sample belonging to class $y$:

# - The parameters $\theta_y$
#  is estimated by a smoothed version of maximum likelihood, i.e. relative frequency counting:
# $\hat{\theta}_{yi} = \frac{ N_{yi} + \alpha}{N_y + \alpha n}$
# - where **$N_{yi} = \sum_{x \in T} x_i$** 
#  is the number of times feature $i$ appears in a sample of class $y$  in the training set , and $N_{y} = \sum_{i=1}^{n} N_{yi}$ is the total count of all features for class .



#%%
print(X_train_tfidf[0].shape, X_train_counts.shape)
print(type(X_train_tfidf[0]))
from sklearn.naive_bayes import MultinomialNB
nb_model = MultinomialNB()
X_tr, X_vl = X_train_tfidf[100:], X_train_tfidf[:100]
y_tr, y_vl = train_df.target.values[100:], train_df.target.values[:100]
nb_model.fit(X_tr, y_tr)
print("Accuracy for MultinomialNB model: ", nb_model.score(X_vl, y_vl))

# %%[markdown]
# ---
## Pipelining 
# In order to make the vectorizer => transformer => classifier easier to work with, scikit-learn provides a Pipeline class that behaves like a compound classifier:

from sklearn.pipeline import Pipeline

text_clf = Pipeline([('vect', CountVectorizer()), ('tfidf', TfidfTransformer()),
('clf', MultinomialNB()) ])

text_clf.fit(train_df.text, train_df.target)
print("Accuracy for pipelined MultinomialNB model: ", text_clf.score(X_vl, y_vl))


# %% [markdown]
# ---
## Support vector machines (SVMs):
# - good for classification, regression and outliers detection.
# - support vector machine (SVM) is regarded as one of the best text classification algorithms

#### The advantages of support vector machines are:

# - Effective in high dimensional spaces.
# Still effective in cases where number of dimensions is greater than the number of samples.
# - Uses a subset of training points in the decision function (called support vectors), so it is also memory efficient.
# - Versatile: different Kernel functions can be specified for the decision function. Common kernels are provided, but it is also possible to specify custom kernels.

#### The disadvantages of support vector machines include:

# - If the number of features is much greater than the number of samples, avoid over-fitting in choosing Kernel functions and regularization term is crucial.
# - SVMs do not directly provide probability estimates, these are calculated using an expensive five-fold cross-validation (see Scores and probabilities, below).
# -  A bit slower than naïve Bayes models.

# %%
from sklearn.linear_model import SGDClassifier
from sklearn import metrics

text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', SGDClassifier(loss='hinge', penalty='l2',
                          alpha=1e-3, random_state=42,
                          max_iter=5, tol=None)),
])

text_clf.fit(train_df.text, train_df.target)

# %%
X_vl = train_df.text[:100]
y_vl = train_df.target[:100]

predicted = text_clf.predict(X_vl)
mean_acc = np.mean(predicted == y_vl)

print("Accuracy for pipelined SVM model: ", mean_acc)

print(metrics.classification_report(X_vl, predicted))




# %%
